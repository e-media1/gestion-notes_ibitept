package com.emedia.gestionnotesibitept.appreciationSanction.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.appreciationSanction.dao.AppreciationSanctionRepository;
import com.emedia.gestionnotesibitept.appreciationSanction.entities.AppreciationSanction;
import com.emedia.gestionnotesibitept.appreciationSanction.entities.AppreciationSanctionKey;

@CrossOrigin("*")
@RestController
public class AppreciationSanctionRestServices {
	@Autowired
	private AppreciationSanctionRepository appreciationsanctionrepository;
	
	@GetMapping(value="/listAppreciation")
	public List<AppreciationSanction> dispensercours(){
		return appreciationsanctionrepository.findAll();
	}
	
	@GetMapping(value="/listAppreciation/{id}")
	public AppreciationSanction listAppreciation(@PathVariable("id") Long id) {
		return appreciationsanctionrepository.findById(id).get();
	}
	
	@PostMapping(value="/listAppreciation")
	public AppreciationSanction save( @RequestBody AppreciationSanction ap){
		return appreciationsanctionrepository.save(ap);
	}
	
	@PutMapping(value="/listAppreciation/{id}")
	public AppreciationSanction update(@PathVariable("id") AppreciationSanctionKey id, @RequestBody AppreciationSanction d){
		d.setId(id); 
		return appreciationsanctionrepository.save(d);
	}
	
	@DeleteMapping(value="/listAppreciation/{id}")
	public void delete(@PathVariable(name="id") Long id) {
		appreciationsanctionrepository.deleteById(id);
	}

}
