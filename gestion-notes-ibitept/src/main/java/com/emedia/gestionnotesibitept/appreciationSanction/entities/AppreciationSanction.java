package com.emedia.gestionnotesibitept.appreciationSanction.entities;

import java.io.Serializable;

import javax.persistence.*;

import com.emedia.gestionnotesibitept.TypeAppreciationSanction.entities.TypeAppreciationSanction;
import com.emedia.gestionnotesibitept.etudiant.entities.Etudiant;
import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;

@Entity
public class AppreciationSanction {
	@EmbeddedId
	AppreciationSanctionKey id;
	
	@ManyToOne
	@MapsId("etudiantId")
	Etudiant etudiant;
	
	@ManyToOne
	@MapsId("typeApprSancId")
	TypeAppreciationSanction typeappreciationsanction;
	
	@ManyToOne
	@MapsId("sessionPromoId")
	SessionPromotion sessionpromotion;
	
	private String dateAppreciation;
	
	AppreciationSanction(){
		
	}
		
		
	public AppreciationSanction(AppreciationSanctionKey id, Etudiant etudiant,
			TypeAppreciationSanction typeappreciationsanction, SessionPromotion sessionpromotion,
			String dateAppreciation, String nomAppreciateur) {
		super();
		this.id = id;
		this.etudiant = etudiant;
		this.typeappreciationsanction = typeappreciationsanction;
		this.sessionpromotion = sessionpromotion;
		this.dateAppreciation = dateAppreciation;
		this.nomAppreciateur = nomAppreciateur;
	}

	private String nomAppreciateur;

	public AppreciationSanctionKey getId() {
		return id;
	}


	public void setId(AppreciationSanctionKey id) {
		this.id = id;
	}


	public Etudiant getEtudiant() {
		return etudiant;
	}


	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}


	public TypeAppreciationSanction getTypeappreciationsanction() {
		return typeappreciationsanction;
	}


	public void setTypeappreciationsanction(TypeAppreciationSanction typeappreciationsanction) {
		this.typeappreciationsanction = typeappreciationsanction;
	}


	public SessionPromotion getSessionpromotion() {
		return sessionpromotion;
	}


	public void setSessionpromotion(SessionPromotion sessionpromotion) {
		this.sessionpromotion = sessionpromotion;
	}


	public String getDateAppreciation() {
		return dateAppreciation;
	}


	public void setDateAppreciation(String dateAppreciation) {
		this.dateAppreciation = dateAppreciation;
	}


	public String getNomAppreciateur() {
		return nomAppreciateur;
	}


	public void setNomAppreciateur(String nomAppreciateur) {
		this.nomAppreciateur = nomAppreciateur;
	}
	
	
}
