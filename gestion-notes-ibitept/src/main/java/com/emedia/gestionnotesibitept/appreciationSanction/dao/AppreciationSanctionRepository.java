package com.emedia.gestionnotesibitept.appreciationSanction.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emedia.gestionnotesibitept.appreciationSanction.entities.AppreciationSanction;

public interface AppreciationSanctionRepository extends JpaRepository<AppreciationSanction, Long> {

}
