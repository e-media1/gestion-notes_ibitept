package com.emedia.gestionnotesibitept.appreciationSanction.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AppreciationSanctionKey implements Serializable {
	
	@Column(name="etudiant_id")
	Long etudiantId;
	
	public Long getEtudiantId() {
		return etudiantId;
	}

	public void setEtudiantId(Long etudiantId) {
		this.etudiantId = etudiantId;
	}

	public Long getTypeApprSancId() {
		return typeApprSancId;
	}

	public void setTypeApprSancId(Long typeApprSancId) {
		this.typeApprSancId = typeApprSancId;
	}

	public Long getSessionPromoId() {
		return sessionPromoId;
	}

	public void setSessionPromoId(Long sessionPromoId) {
		this.sessionPromoId = sessionPromoId;
	}

	public AppreciationSanctionKey() {
		
	}
	
	public AppreciationSanctionKey(Long etudiantId, Long typeApprSancId, Long sessionPromoId) {
		super();
		this.etudiantId = etudiantId;
		this.typeApprSancId = typeApprSancId;
		this.sessionPromoId = sessionPromoId;
	}

	@Column(name="type_appre_sanc_id")
	Long typeApprSancId;
	
	@Column(name="session_promo_id")
	Long sessionPromoId;
	

}
