package com.emedia.gestionnotesibitept.matiere.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import com.emedia.gestionnotesibitept.dispenserCours.entities.DispenserCours;
import com.emedia.gestionnotesibitept.enseignant.entities.Enseignant;
import com.emedia.gestionnotesibitept.evaluation.entities.Evaluation;
import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;
import com.emedia.gestionnotesibitept.session.entities.Session;

@Entity
@Table(name = "matiere")
public class Matiere implements Serializable {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "codeMatiere", nullable = false,unique = true)
	private int codeMatiere;
	
	@Column(name = "libelleMatiere", nullable = false, unique = true)
	private String libelleMatiere;
	
	@Column(name = "coefMatiere", nullable = false)
	private int coefMatiere;
	
	@Column(name = "dateDebutMatiere", nullable = false)
	private String dateDebutMatiere;
	
	@Column(name = "dateFinMatiere", nullable = false)
	private String dateFinMatiere;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public Enseignant enseignant;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public Session session;
	

	public Matiere() {

	}

	public Matiere(Long id, int codeMatiere, String libelleMatiere, int coefMatiere, String dateDebutMatiere,
			String dateFinMatiere, Enseignant enseignant, Session session) {
		super();
		this.id = id;
		this.codeMatiere = codeMatiere;
		this.libelleMatiere = libelleMatiere;
		this.coefMatiere = coefMatiere;
		this.dateDebutMatiere = dateDebutMatiere;
		this.dateFinMatiere = dateFinMatiere;
		this.enseignant = enseignant;
		this.session = session;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCodeMatiere() {
		return codeMatiere;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public void setCodeMatiere(int codeMatiere) {
		this.codeMatiere = codeMatiere;
	}

	public String getLibelleMatiere() {
		return libelleMatiere;
	}

	public void setLibelleMatiere(String libelleMatiere) {
		this.libelleMatiere = libelleMatiere;
	}

	public int getCoefMatiere() {
		return coefMatiere;
	}

	public void setCoefMatiere(int coefMatiere) {
		this.coefMatiere = coefMatiere;
	}

	public String getDateDebutMatiere() {
		return dateDebutMatiere;
	}

	public void setDateDebutMatiere(String dateDebutMatiere) {
		this.dateDebutMatiere = dateDebutMatiere;
	}

	public String getDateFinMatiere() {
		return dateFinMatiere;
	}

	public void setDateFinMatiere(String dateFinMatiere) {
		this.dateFinMatiere = dateFinMatiere;
	}

	@Override
	public String toString() {
		return "Matiere [id=" + id + ", CodeMatiere=" + codeMatiere + ", LibelleMatiere=" + libelleMatiere
				+ ", CoefMatiere=" + coefMatiere + ", DateDebutMatiere=" + dateDebutMatiere + ", DateFinMatiere="
				+ dateFinMatiere + ", enseignant=" + enseignant + ", session=" + session + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(codeMatiere, coefMatiere, dateDebutMatiere, dateFinMatiere, libelleMatiere, enseignant, id,
				session);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matiere other = (Matiere) obj;
		return codeMatiere == other.codeMatiere && coefMatiere == other.coefMatiere
				&& Objects.equals(dateDebutMatiere, other.dateDebutMatiere)
				&& Objects.equals(dateFinMatiere, other.dateFinMatiere)
				&& Objects.equals(libelleMatiere, other.libelleMatiere) && Objects.equals(enseignant, other.enseignant)
				&& Objects.equals(id, other.id) && Objects.equals(session, other.session);
	}
	
	
}
