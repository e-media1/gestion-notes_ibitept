package com.emedia.gestionnotesibitept.matiere.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.matiere.entities.Matiere;



@RepositoryRestResource
public interface MatiereRepository extends JpaRepository<Matiere, Long> {

}

