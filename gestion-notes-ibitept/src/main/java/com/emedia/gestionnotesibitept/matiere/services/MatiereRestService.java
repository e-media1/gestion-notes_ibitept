package com.emedia.gestionnotesibitept.matiere.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.emedia.gestionnotesibitept.matiere.dao.MatiereRepository;
import com.emedia.gestionnotesibitept.matiere.entities.Matiere;



@RestController
@CrossOrigin("*")
public class MatiereRestService {
	@Autowired
	MatiereRepository matiereRepository;
	
	
	@GetMapping(value = "/listMatiere")
	public List<Matiere> listMatiere(){
		return matiereRepository.findAll();
	}
	
	@GetMapping(value = "/listMatiere/{id}")
	public Matiere listMatiere(@PathVariable(name = "id") Long id){
		return matiereRepository.findById(id).get();
	}
	

	@PostMapping(value = "/saveMatiere")
	public Matiere saveMatiere(@RequestBody Matiere m){
		return matiereRepository.save(m);
	}
	

	@PutMapping(value = "/updateMatiere/{id}")
	public Matiere updateMatiere(@PathVariable(name = "id") Long id,@RequestBody Matiere m){
		m.setId(id);
		return matiereRepository.save(m);
	}
	
	@DeleteMapping(value = "/deleteMatiere/{id}")
	public void updateMatiere(@PathVariable(name = "id") Long id){
		 matiereRepository.deleteById(id);
	}
	
	
}

