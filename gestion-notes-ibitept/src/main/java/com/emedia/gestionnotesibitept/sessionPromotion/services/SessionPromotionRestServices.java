package com.emedia.gestionnotesibitept.sessionPromotion.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.sessionPromotion.dao.SessionPromotionRepository;
import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;

@CrossOrigin("*")
@RestController
public class SessionPromotionRestServices {
	@Autowired
	private SessionPromotionRepository sessionpromotionRepository;

	@GetMapping(value = "/listSessionsPromotions")
	public List<SessionPromotion> listSessionsPromotions() {
		return sessionpromotionRepository.findAll();

	}

	@GetMapping(value = "/listSessionsPromotions/{id}")
	public SessionPromotion listSessionsPromotions(@PathVariable(name = "id") Long id) {
		return sessionpromotionRepository.findById(id).get();

	}

	
	@PostMapping(value="/saveSessionsPromotions")
	public SessionPromotion save(@RequestBody SessionPromotion s){
		return sessionpromotionRepository.save(s);

	}

	@PutMapping(value = "/updateSessionsPromotions/{id}")
	public SessionPromotion update(@PathVariable(name = "id") Long id, @RequestBody SessionPromotion s) {
		s.setId(id);
		return sessionpromotionRepository.save(s);

	}

	@DeleteMapping(value = "/deleteSessionsPromotions/{id}")
	public void delete(@PathVariable(name = "id") Long id) {

		sessionpromotionRepository.deleteById(id);

	}

}
