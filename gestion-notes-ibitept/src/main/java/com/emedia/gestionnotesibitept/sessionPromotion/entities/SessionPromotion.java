package com.emedia.gestionnotesibitept.sessionPromotion.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.emedia.gestionnotesibitept.appreciationSanction.entities.AppreciationSanction;
import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;
import com.emedia.gestionnotesibitept.promotion.entities.Promotion;
import com.emedia.gestionnotesibitept.session.entities.Session;

import lombok.Data;

@Entity
@Data
public class SessionPromotion implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	Long id;
	
	@Column(name = "rang", nullable = false)
	int rang;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public Promotion promotion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public Session session;

	
//	@OneToMany(mappedBy = "sessionPromotion")
//	private List<ProgrammeCours> programmecours;
//	
//	@OneToMany(mappedBy = "sessionpromotion")
//    private List<AppreciationSanction> appreciationSanction;
//	




//	@OneToMany(mappedBy = "sessionpromotion")
//	private List<ProgrammeCours> programmeCours;
//
//
//
//	@OneToMany(mappedBy = "sessionpromotion")
//	private List<AppreciationSanction> appreciationSanction;


	public SessionPromotion() {

	}

	public SessionPromotion(Long id, int rang, String titreRang, Promotion promotion, Session session) {
		super();
		this.id = id;
		this.rang = rang;
		this.promotion = promotion;
		this.session = session;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public String toString() {
		return "SessionPromotion [id=" + id + ", rang=" + rang + ", promotion=" + promotion + ", session=" + session
				+ "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, promotion, rang, session);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionPromotion other = (SessionPromotion) obj;
		return Objects.equals(id, other.id) && Objects.equals(promotion, other.promotion) && rang == other.rang
				&& Objects.equals(session, other.session);
	}

	
}
