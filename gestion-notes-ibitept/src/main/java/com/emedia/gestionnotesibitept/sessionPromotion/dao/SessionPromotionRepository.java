package com.emedia.gestionnotesibitept.sessionPromotion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;

@RepositoryRestResource
public interface SessionPromotionRepository extends JpaRepository<SessionPromotion,Long> {

}
