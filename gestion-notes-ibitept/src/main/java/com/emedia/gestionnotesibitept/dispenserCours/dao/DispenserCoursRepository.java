package com.emedia.gestionnotesibitept.dispenserCours.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.dispenserCours.entities.DispenserCours;



@RepositoryRestResource
public interface DispenserCoursRepository extends JpaRepository<DispenserCours, Long> {

}

