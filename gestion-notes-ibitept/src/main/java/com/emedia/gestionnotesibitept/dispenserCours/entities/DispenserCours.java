package com.emedia.gestionnotesibitept.dispenserCours.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.emedia.gestionnotesibitept.enseignant.entities.Enseignant;
import com.emedia.gestionnotesibitept.matiere.entities.Matiere;

@Entity

public class DispenserCours {
	
	@EmbeddedId
	DispenserCoursKey id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	@MapsId("enseignantId")
	Enseignant enseignant;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	@MapsId("matiereId")
	Matiere matiere;
	
	public DispenserCours() {
		super();
	}
	
	public DispenserCours(DispenserCoursKey id, Enseignant enseignant, Matiere matiere, String dateDebutProgramme) {
		super();
		this.id = id;
		this.enseignant = enseignant;
		this.matiere = matiere;
		this.dateDebutProgramme = dateDebutProgramme;
	}

	
	public DispenserCoursKey getId() {
		return id;
	}

	public void setId(DispenserCoursKey id2) {
		this.id = id2;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public String getDateDebutProgramme() {
		return dateDebutProgramme;
	}

	public void setDateDebutProgramme(String dateDebutProgramme) {
		this.dateDebutProgramme = dateDebutProgramme;
	}

	private String dateDebutProgramme;
	
	

	
}
