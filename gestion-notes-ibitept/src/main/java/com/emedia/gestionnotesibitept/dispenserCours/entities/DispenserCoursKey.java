package com.emedia.gestionnotesibitept.dispenserCours.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DispenserCoursKey implements Serializable {
	
	@Column(name="enseignant_id")
	Long enseignantId;
	
	@Column(name="matiere_id")
	Long matiereId;
	

}
