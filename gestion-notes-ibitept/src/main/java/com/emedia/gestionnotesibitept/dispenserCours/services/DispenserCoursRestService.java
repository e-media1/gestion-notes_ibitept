package com.emedia.gestionnotesibitept.dispenserCours.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.dispenserCours.dao.DispenserCoursRepository;
import com.emedia.gestionnotesibitept.dispenserCours.entities.DispenserCours;
import com.emedia.gestionnotesibitept.dispenserCours.entities.DispenserCoursKey;

@CrossOrigin("*")
@RestController
public class DispenserCoursRestService {

	@Autowired
	private DispenserCoursRepository dispensercoursrepository;

	@GetMapping(value = "/listDispense")
	public List<DispenserCours> dispensercours() {
		return dispensercoursrepository.findAll();
	}

	@GetMapping(value = "/listDispense/{id}")
	public DispenserCours listCoursDispenser(@PathVariable("id") Long id) {
		return dispensercoursrepository.findById(id).get();
	}

	@PostMapping(value = "/listDispense")
	public DispenserCours save(@RequestBody DispenserCours ap) {
		return dispensercoursrepository.save(ap);
	}

	@PutMapping(value = "/listDispense/{id}")
	public DispenserCours update(@PathVariable("id") DispenserCoursKey id, @RequestBody DispenserCours d) {
		d.setId(id);
		return dispensercoursrepository.save(d);
	}

	@DeleteMapping(value = "/listDispense/{id}")
	public void delete(@PathVariable(name = "id") Long id) {
		dispensercoursrepository.deleteById(id);
	}
}
