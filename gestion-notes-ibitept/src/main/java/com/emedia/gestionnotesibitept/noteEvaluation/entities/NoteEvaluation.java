package com.emedia.gestionnotesibitept.noteEvaluation.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.emedia.gestionnotesibitept.etudiant.entities.Etudiant;
import com.emedia.gestionnotesibitept.evaluation.entities.Evaluation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
public class NoteEvaluation implements Serializable {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "noteEtudiant", nullable = false)
	private Double noteEtudiant;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
        @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Etudiant etudiant;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	private Evaluation evaluation;

	// getters and setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getNoteEtudiant() {
		return noteEtudiant;
	}

	public void setNoteEtudiant(Double noteEtudiant) {
		this.noteEtudiant = noteEtudiant;
	}

	// Constructors
	public NoteEvaluation() {

	}

	public NoteEvaluation(Long id, Double noteEtudiant, Etudiant etudiant, Evaluation evaluation) {

		this.id = id;
		this.noteEtudiant = noteEtudiant;
		this.etudiant = etudiant;
		this.evaluation = evaluation;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Evaluation getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}

	@Override
	public String toString() {
		return "NoteEvaluation [id=" + id + ", noteEtudiant=" + noteEtudiant + ", etudiant=" + etudiant
				+ ", evaluation=" + evaluation + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(etudiant, evaluation, id, noteEtudiant);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoteEvaluation other = (NoteEvaluation) obj;
		return Objects.equals(etudiant, other.etudiant) && Objects.equals(evaluation, other.evaluation)
				&& Objects.equals(id, other.id) && Objects.equals(noteEtudiant, other.noteEtudiant);
	}

}
