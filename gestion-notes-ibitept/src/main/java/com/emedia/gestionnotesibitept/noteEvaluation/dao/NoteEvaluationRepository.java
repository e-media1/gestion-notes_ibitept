package com.emedia.gestionnotesibitept.noteEvaluation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.noteEvaluation.entities.NoteEvaluation;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@RepositoryRestResource
public interface NoteEvaluationRepository extends JpaRepository<NoteEvaluation, Long> {
//@Query(value="SELECT t from enseignant, evaluation, etudiant, matiere, programme_cours, promotion, session_promotion, session, note_evaluation note WHERE etudiant_id = :etudiantId")
//List<NoteEvaluation> findbyetudiant(@Param("idetudiant") Long etudiantId);

//@Query(value="SELECT note From NoteEvaluation note WHERE note.etudiant.id = :etudiantId")
//List<NoteEvaluation> findByEtudiant(@Param("etudiantId") Long etudiantId); AND note.id = pc.id AND pc.id = sp.id
    
@Query(value="SELECT note From NoteEvaluation note WHERE note.etudiant.id = :etudiantId AND note.evaluation.id = :evaluationId")       
List<NoteEvaluation> findByEtudiant(@Param("etudiantId") Long etudiantId, @Param("evaluationId") Long evaluationId);

}
