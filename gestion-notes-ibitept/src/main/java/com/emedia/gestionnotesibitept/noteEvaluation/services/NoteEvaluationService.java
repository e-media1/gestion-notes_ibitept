package com.emedia.gestionnotesibitept.noteEvaluation.services;

import com.emedia.gestionnotesibitept.etudiant.dao.EtudiantRepository;
import com.emedia.gestionnotesibitept.etudiant.entities.Etudiant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.emedia.gestionnotesibitept.noteEvaluation.dao.NoteEvaluationRepository;
import com.emedia.gestionnotesibitept.noteEvaluation.entities.NoteEvaluation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@CrossOrigin("*")
@RestController
public class NoteEvaluationService {
	
	@Autowired
	private NoteEvaluationRepository noteEvaluationRepository;
	@Autowired
	private EtudiantRepository etudiantRepository;
        
	@GetMapping(value="/listNoteEvaluation")
	public List<NoteEvaluation> listNoteEvaluation() {
		return noteEvaluationRepository.findAll();
	}
	
	@GetMapping(value="/listNoteEvaluation/{id}")
	public NoteEvaluation listNoteEvaluations(@PathVariable("id") Long id){
		return noteEvaluationRepository.findById(id).get();
	}
	
	@PutMapping(value="/updateNoteEvaluation/{id}")
	public NoteEvaluation update(@PathVariable("id") Long id, @RequestBody NoteEvaluation ne){
		ne.setId(id); 
                  System.out.println("Modifier NoteEvaluation ==="+ ne);
		return noteEvaluationRepository.save(ne);
	}
	
//	@PostMapping(value="/listNoteEvaluation")
//	public NoteEvaluation update( @RequestBody NoteEvaluation ne){
//		return noteEvaluationRepository.save(ne);
//	}
	
	@PostMapping(value="/saveNoteEvaluation")
	public NoteEvaluation save( @RequestBody NoteEvaluation ne){
            System.out.println("Ajout NoteEvaluation ==="+ ne);
		return noteEvaluationRepository.save(ne);
	}
	
	@DeleteMapping(value="/deleteNoteEvaluation/{id}")
	public void delete(@PathVariable(name="id") Long id) {
		noteEvaluationRepository.deleteById(id);
	}
	@RequestMapping(value="/listNoteEvaluation/search")
	public List<NoteEvaluation> affciher(@RequestParam(name="idEtudiant") Long idEtudiant, @RequestParam(name="idEvaluation") Long idEvaluation){
		Etudiant returnValue= etudiantRepository.getById(idEtudiant);
                
		System.out.println(returnValue.getEmailEtudiant());
		return noteEvaluationRepository.findByEtudiant(idEtudiant,idEvaluation);
			
	}
	
	

}
