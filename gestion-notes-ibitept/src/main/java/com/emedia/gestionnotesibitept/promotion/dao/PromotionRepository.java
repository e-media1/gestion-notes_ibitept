package com.emedia.gestionnotesibitept.promotion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.promotion.entities.Promotion;

@RepositoryRestResource
public interface PromotionRepository extends JpaRepository<Promotion, Long> {

}
