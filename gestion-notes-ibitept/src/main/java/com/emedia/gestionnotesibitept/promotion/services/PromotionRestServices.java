package com.emedia.gestionnotesibitept.promotion.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.promotion.dao.PromotionRepository;
import com.emedia.gestionnotesibitept.promotion.entities.Promotion;


@CrossOrigin("*")
@RestController
public class PromotionRestServices {
	@Autowired
	private PromotionRepository promotionRepository;
	
	@GetMapping(value="/listPromotions")
	public List<Promotion> listPromotions(){
		return promotionRepository.findAll();
	}
	
	@GetMapping(value="/listPromotions/{id}")
	public Promotion listPromotions(@PathVariable(name="id") Long id){
		return promotionRepository.findById(id).get();
	}
	
	@PostMapping(value="/savePromotion")
	public Promotion save(@RequestBody Promotion p) {
		return promotionRepository.save(p);
		
	}
	
	@PutMapping(value="/updatePromotion/{id}")
	public Promotion update(@PathVariable(name="id") Long id, @RequestBody Promotion p) {
		p.setId(id);
		return promotionRepository.save(p);
	}
	
	@DeleteMapping(value="/deletePromotion/{id}")
	public void delete(@PathVariable(name="id") Long id) {
		promotionRepository.deleteById(id);
	}
	
}


