package com.emedia.gestionnotesibitept.promotion.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;

import javax.persistence.OneToMany;

import com.emedia.gestionnotesibitept.AppartenirPromotion.entities.AppartenirPromotion;
import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;

import lombok.Data;


@Entity
@Data 
public class Promotion implements Serializable {
	

	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;


	@Column(name = "nom", nullable = false)


//	@OneToMany(mappedBy = "promotion" )
//	private List<SessionPromotion> sessionpromotions;
//	
//	
//	
//	@OneToMany(mappedBy = "etudiant")
//	private List<AppartenirPromotion> appartenirPromotion;

	
private String nom;
	
	@Column(name = "date_debut", nullable = false)
	private String date_debut;
	
	@Column(name = "date_fin", nullable = false)
	private String date_fin;
	
	
	//constructors

	public Promotion() {
		
	}
	
	public Promotion(Long id, String nom, String date_debut, String date_fin) {

		super();
		this.id = id;
		this.nom = nom;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
	}

	

	//Getters and setters
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(String date_debut) {
		this.date_debut = date_debut;
	}
	public String getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(String date_fin) {
		this.date_fin = date_fin;
	}

	@Override
	public String toString() {
		return "Promotion [id=" + id + ", nom=" + nom + ", date_debut=" + date_debut + ", date_fin=" + date_fin + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(date_debut, date_fin, id, nom);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion other = (Promotion) obj;
		return Objects.equals(date_debut, other.date_debut) && Objects.equals(date_fin, other.date_fin)
				&& Objects.equals(id, other.id) && Objects.equals(nom, other.nom);
	}

	

}
