package com.emedia.gestionnotesibitept.programmeCours.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;


@RepositoryRestResource
public interface ProgrammeCoursRepository extends JpaRepository<ProgrammeCours, Long> {

}