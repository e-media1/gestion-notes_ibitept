package com.emedia.gestionnotesibitept.programmeCours.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.emedia.gestionnotesibitept.programmeCours.dao.ProgrammeCoursRepository;
import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;

@CrossOrigin("*")
@RestController
public class ProgrammeCoursRestService{
 
	@Autowired
	ProgrammeCoursRepository programmeCourRepository;
	
	@GetMapping(value = "/listProgramme_cour")
	public List<ProgrammeCours> listProgramme_cour() {
		return programmeCourRepository.findAll();
	}
	
	@GetMapping(value = "/listProgramme_cour/{id}")
	public ProgrammeCours listProgrammeCour(@PathVariable(name="id") Long id) {
		return programmeCourRepository.findById(id).get();
	}
	
	@PostMapping(value = "/saveProgrammeCour")
	public ProgrammeCours saveProgrammeCour(@RequestBody ProgrammeCours pc) {
		return programmeCourRepository.save(pc);
	}
	

	@PutMapping(value = "/updateProgrammeCour/{id}")
	public ProgrammeCours updateProgrammeCour(@PathVariable(name="id") Long id ,@RequestBody ProgrammeCours pc) {
		pc.setId(id);
		return programmeCourRepository.save(pc);
	}
	
	@DeleteMapping(value = "/deleteProgrammeCour/{id}")
	public void deleteProgrammeCour(@PathVariable(name="id") Long id) {
		programmeCourRepository.deleteById(id);
	}
}
