package com.emedia.gestionnotesibitept.programmeCours.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import com.emedia.gestionnotesibitept.enseignant.entities.Enseignant;
import com.emedia.gestionnotesibitept.evaluation.entities.Evaluation;
import com.emedia.gestionnotesibitept.matiere.entities.Matiere;
import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;

@Entity
public class ProgrammeCours implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "coef", nullable = false)
	private int coef;
	
	@Column(name = "date_debut_cours", nullable = false)
	private String dateDebutCours;
	
	@Column(name = "date_fin_cours", nullable = false)
	private String dateFinCours;
	
	@Column(name = "observation_cours", nullable = false)
	private String observationCours;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public Matiere matiere;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public Enseignant enseignant;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public SessionPromotion sessionPromotion;
	

//	@OneToMany(mappedBy = "programmeCours",cascade = CascadeType.ALL)
//	public List<Evaluation> evaluations;


	public ProgrammeCours() {
		super();
	}

	public ProgrammeCours(Long id, int coef, String dateDebutCours, String dateFinCours, String observationCours,
			Matiere matiere, Enseignant enseignant, SessionPromotion sessionPromotion) {
		super();
		this.id = id;
		this.coef = coef;
		this.dateDebutCours = dateDebutCours;
		this.dateFinCours = dateFinCours;
		this.observationCours = observationCours;
		this.matiere = matiere;
		this.enseignant = enseignant;
		this.sessionPromotion = sessionPromotion;
		
	}


//	public Long getId() {
//		return id;
//
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCoef() {
		return coef;
	}

	public void setCoef(int coef) {
		this.coef = coef;
	}

	public String getDateDebutCours() {
		return dateDebutCours;
	}

	public void setDateDebutCours(String dateDebutCours) {
		this.dateDebutCours = dateDebutCours;
	}

	public String getDateFinCours() {
		return dateFinCours;
	}

	public void setDateFinCours(String dateFinCours) {
		this.dateFinCours = dateFinCours;
	}

	public String getObservationCours() {
		return observationCours;
	}

	public void setObservationCours(String observationCours) {
		this.observationCours = observationCours;
	}

//	public void setId(Long id) {
//		// TODO Auto-generated method stub
//
//	}
	public SessionPromotion getSessionpromotion() {
		return sessionPromotion;
	}

	public void setSessionpromotion(SessionPromotion sessionpromotion) {
		this.sessionPromotion = sessionpromotion;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	@Override
	public String toString() {
		return "ProgrammeCours [id=" + id + ", coef=" + coef + ", dateDebutCours=" + dateDebutCours + ", dateFinCours="
				+ dateFinCours + ", observationCours=" + observationCours + ", matiere=" + matiere + ", enseignant="
				+ enseignant + ", sessionpromotion=" + sessionPromotion + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(coef, dateDebutCours, dateFinCours, enseignant, id, matiere, observationCours,
				sessionPromotion);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgrammeCours other = (ProgrammeCours) obj;
		return coef == other.coef && Objects.equals(dateDebutCours, other.dateDebutCours)
				&& Objects.equals(dateFinCours, other.dateFinCours) && Objects.equals(enseignant, other.enseignant)
				&& Objects.equals(id, other.id) && Objects.equals(matiere, other.matiere)
				&& Objects.equals(observationCours, other.observationCours)
				&& Objects.equals(sessionPromotion, other.sessionPromotion);
	}
	
	
}
