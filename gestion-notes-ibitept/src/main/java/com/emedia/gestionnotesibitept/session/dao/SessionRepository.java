package com.emedia.gestionnotesibitept.session.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.session.entities.Session;



@RepositoryRestResource
public interface SessionRepository extends JpaRepository<Session, Long> {

}
