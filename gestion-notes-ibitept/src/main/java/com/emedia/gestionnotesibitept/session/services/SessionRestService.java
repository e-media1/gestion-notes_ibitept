package com.emedia.gestionnotesibitept.session.services;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import com.emedia.gestionnotesibitept.session.dao.SessionRepository;
import com.emedia.gestionnotesibitept.session.entities.Session;


@CrossOrigin("*")
@RestController
public class  SessionRestService  {

	@Autowired
	SessionRepository sessionRepository;
	
	@GetMapping(value = "/listSession")
	public List<Session> listSession(){
		return sessionRepository.findAll();
	}
	
	@GetMapping(value = "/listSession/{id}")
	public Session listSession(@PathVariable(name="id") Long id) {
		return sessionRepository.findById(id).get();
	}
	
	@PostMapping(value = "/saveSession")
	public Session saveSession(@RequestBody Session se) {
		return sessionRepository.save(se);
	}
	
	@PutMapping(value = "/updateSession/{id}")
	public Session updateSession(@PathVariable(name="id") Long id,@RequestBody Session se) {
		se.setId(id);
		return sessionRepository.save(se);
	}
	
	@DeleteMapping(value = "/deleteSession/{id}")
	public void deleteSession(@PathVariable(name="id") Long id) {
		 sessionRepository.deleteById(id);
	}
}
