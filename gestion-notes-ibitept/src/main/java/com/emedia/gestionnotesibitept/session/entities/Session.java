package com.emedia.gestionnotesibitept.session.entities;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;
import com.emedia.gestionnotesibitept.matiere.entities.Matiere;
import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;


@Entity
public class Session implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "annee_session", nullable = false)
	private double anneeSession;
	
	@Column(name = "rang_session", nullable = false)
	private int rangSession;
	
	@Column(name = "date_debut_session", nullable = false)
	private String dateDebutSession;
	
	@Column(name = "date_fin_session", nullable = false)
	private String dateFinSession;
	
//	@OneToMany(mappedBy = "session")
//	private List<Matiere> matieres;
//	
//	@OneToMany(mappedBy = "session")
//	List<SessionPromotion> sessionPromotion;
//	
	public Session() {
		
	}


	public Session(Long id, double anneeSession, int rangSession, String dateDebutsession, String dateFinsession) {
		super();
		this.id = id;
		this.anneeSession = anneeSession;
		this.rangSession = rangSession;
		this.dateDebutSession = dateDebutsession;
		this.dateFinSession = dateFinsession;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public double getAnneeSession() {
		return anneeSession;
	}


	public void setAnneeSession(double anneeSession) {
		this.anneeSession = anneeSession;
	}


	public int getRangSession() {
		return rangSession;
	}


	public void setRangSession(int rangSession) {
		this.rangSession = rangSession;
	}


	public String getDateDebutsession() {
		return dateDebutSession;
	}


	public void setDateDebutsession(String dateDebutsession) {
		this.dateDebutSession = dateDebutsession;
	}


	public String getDateFinsession() {
		return dateFinSession;
	}


	public void setDateFinsession(String dateFinsession) {
		this.dateFinSession = dateFinsession;
	}


	@Override
	public String toString() {
		return "Session [id=" + id + ", AnneeSession=" + anneeSession + ", RangSession=" + rangSession
				+ ", DateDebutsession=" + dateDebutSession + ", DateFinsession=" + dateFinSession + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(anneeSession, dateDebutSession, dateFinSession, rangSession, id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Session other = (Session) obj;
		return Double.doubleToLongBits(anneeSession) == Double.doubleToLongBits(other.anneeSession)
				&& Objects.equals(dateDebutSession, other.dateDebutSession)
				&& Objects.equals(dateFinSession, other.dateFinSession) && rangSession == other.rangSession
				&& Objects.equals(id, other.id);
	}
	
	
}

