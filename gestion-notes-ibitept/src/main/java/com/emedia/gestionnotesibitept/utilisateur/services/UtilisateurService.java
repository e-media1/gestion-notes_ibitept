package com.emedia.gestionnotesibitept.utilisateur.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.utilisateur.dao.UtilisateurRepository;
import com.emedia.gestionnotesibitept.utilisateur.entities.Utilisateur;


@CrossOrigin("*")
@RestController
public class UtilisateurService {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@GetMapping(value="/listUtilisateur")
	public List<Utilisateur> listUtilisateurs() {
		return utilisateurRepository.findAll();
	}
	
	@GetMapping(value="/listUtilisateur/{id}")
	public Utilisateur listUtilisateurs(@PathVariable("id") Long id){
		return utilisateurRepository.findById(id).get();
	}
	
	@PutMapping(value="/listUtilisateur/{id}")
	public Utilisateur update(@PathVariable("id") Long id, @RequestBody Utilisateur u){
		u.setId(id); 
		return utilisateurRepository.save(u);
	}
	
	@PostMapping(value="/listUtilisateur")
	public Utilisateur update( @RequestBody Utilisateur u){
		return utilisateurRepository.save(u);
	}
}
