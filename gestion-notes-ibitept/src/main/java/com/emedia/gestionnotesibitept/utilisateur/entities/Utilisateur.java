package com.emedia.gestionnotesibitept.utilisateur.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;

@Entity
@Data @ToString
public class Utilisateur implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nomUtilisateur;
	private String motDepasse;
	private String dateCreation;
	
	//Getters
	
	public Long getId() {
		return id;
	}
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	public String getMotDepasse() {
		return motDepasse;
	}
	public String getDateCreation() {
		return dateCreation;
	}
	
	
	//Setters
	
	
	public void setId(Long id) {
		this.id = id;
	}
	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}
	public void setMotDepasse(String motDepasse) {
		this.motDepasse = motDepasse;
	}
	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	//Constructors
	public Utilisateur(Long id, String nomUtilisateur, String motDepasse, String dateCreation) {
		super();
		this.id = id;
		this.nomUtilisateur = nomUtilisateur;
		this.motDepasse = motDepasse;
		this.dateCreation = dateCreation;
	}
	
	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

}
