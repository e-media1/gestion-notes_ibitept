package com.emedia.gestionnotesibitept.TypeAppreciationSanction.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.emedia.gestionnotesibitept.appreciationSanction.entities.AppreciationSanction;

import lombok.Data;
import lombok.ToString;

@Entity
@Data @ToString
public class TypeAppreciationSanction implements Serializable {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long codeType;
	private String LibelleType;


	
	@OneToMany(mappedBy = "typeappreciationsanction")
    private List<AppreciationSanction> appreciationSanction;
	
	//getters
	
	public Long getId() {
		return id;
	}
	public Long getCodeType() {
		return codeType;
	}
	public String getLibelleType() {
		return LibelleType;
	}
	

	
	//setters
	
	
	public void setId(Long id) {
		this.id = id;
	}
	public void setCodeType(Long codeType) {
		this.codeType = codeType;
	}
	public void setLibelleType(String libelleType) {
		LibelleType = libelleType;
	}
	
	
	
	
	//constructors
	
	public TypeAppreciationSanction() {
		super();
		
	}
	
	public TypeAppreciationSanction(long id, long codeType, String libelleType) {
		super();
		this.id = id;
		this.codeType = codeType;
		LibelleType = libelleType;

	}
	
	
	
	
	
	
	
	
	
	
	
	

}
