package com.emedia.gestionnotesibitept.TypeAppreciationSanction.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.TypeAppreciationSanction.dao.TypeAppreciationSanctionRepository;
import com.emedia.gestionnotesibitept.TypeAppreciationSanction.entities.TypeAppreciationSanction;


@CrossOrigin("*")
@RestController
public class TypeAppreciationSanctionService {

	@Autowired
	private TypeAppreciationSanctionRepository typeAppreciationSanctionRepository;

	@GetMapping(value = "/apprSanction")
	public List<TypeAppreciationSanction> listTypeAppreciationSanction() {
		return typeAppreciationSanctionRepository.findAll();
	}

	@GetMapping(value = "/apprSanction/{id}")
	public TypeAppreciationSanction listTypeAppreciationSanction(@PathVariable("id") Long id) {
		return typeAppreciationSanctionRepository.findById(id).get();
	}

	@PutMapping(value = "/apprSanction/{id}")
	public TypeAppreciationSanction update(@PathVariable("id") Long id, @RequestBody TypeAppreciationSanction t) {
		t.setId(id);
		return typeAppreciationSanctionRepository.save(t);
	}

	@PostMapping(value = "/apprSanction")
	public TypeAppreciationSanction update(@RequestBody TypeAppreciationSanction t) {
		return typeAppreciationSanctionRepository.save(t);
	}

	@DeleteMapping(value = "/apprSanction/{id}")
	public void delete(@PathVariable(name = "id") Long id) {
		typeAppreciationSanctionRepository.deleteById(id);
	}

}
