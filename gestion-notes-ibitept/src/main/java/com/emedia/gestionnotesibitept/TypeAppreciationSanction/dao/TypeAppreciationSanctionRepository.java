package com.emedia.gestionnotesibitept.TypeAppreciationSanction.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.TypeAppreciationSanction.entities.TypeAppreciationSanction;

@RepositoryRestResource
public interface TypeAppreciationSanctionRepository extends JpaRepository<TypeAppreciationSanction, Long> {
	
}
