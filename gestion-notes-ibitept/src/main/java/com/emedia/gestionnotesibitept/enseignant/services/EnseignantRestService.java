package com.emedia.gestionnotesibitept.enseignant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.emedia.gestionnotesibitept.enseignant.dao.EnseignantRepository;
import com.emedia.gestionnotesibitept.enseignant.entities.Enseignant;

@CrossOrigin("*")
@RestController
public class EnseignantRestService {
	@Autowired
	EnseignantRepository enseignantRepository ;

	@GetMapping(value = "/listEnseignant")
	public List<Enseignant> listEnseignant() {
		return enseignantRepository.findAll();
	}

	@GetMapping(value = "/listEnseignant/{id}")
	public Enseignant listEnseignant(@PathVariable(name = "id") Long id) {
		return enseignantRepository.findById(id).get();
	}

	@PostMapping(value = "/saveEnseignant")
	public Enseignant saveEnseignant(@RequestBody Enseignant e) {
		return enseignantRepository.save(e);
	}

	@PutMapping(value = "/updateEnseignant/{id}")
	public Enseignant updateEnseignant(@PathVariable(name = "id") Long id, @RequestBody Enseignant e) {
		e.setId(id);
		return enseignantRepository.save(e);
	}

	@DeleteMapping(value = "/deleteEnseignant/{id}")
	public void deleteEnseignant(@PathVariable(name = "id") Long id) {
		enseignantRepository.deleteById(id);
	}
}
