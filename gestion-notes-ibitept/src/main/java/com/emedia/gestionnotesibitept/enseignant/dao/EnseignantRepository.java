package com.emedia.gestionnotesibitept.enseignant.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.enseignant.entities.Enseignant;




@RepositoryRestResource
public interface EnseignantRepository extends JpaRepository<Enseignant, Long> {

}

