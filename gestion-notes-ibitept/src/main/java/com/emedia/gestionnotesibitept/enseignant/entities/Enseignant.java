package com.emedia.gestionnotesibitept.enseignant.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import com.emedia.gestionnotesibitept.dispenserCours.entities.DispenserCours;
import com.emedia.gestionnotesibitept.matiere.entities.Matiere;
import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;

@Entity
public class Enseignant implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5142097671438147625L;
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "matriculeEnseignant", nullable = false)
	private int matriculeEnseignant;
	
	@Column(name = "nomEnseignant", nullable = false)
	private String nomEnseignant;
	
	@Column(name = "prenomEnseignant", nullable = false)
	private String prenomEnseignant;
	
	@Column(name = "emailEnseignant", nullable = false)
	private String emailEnseignant;
	
	@Column(name = "telephoneEnseignant", nullable = false)
	private String telephoneEnseignant;
	
	@Column(name = "villeEnseignant", nullable = false)
	private String villeEnseignant;
	
	@Column(name = "paysEnseignant", nullable = false)
	private String paysEnseignant;
	
	@Column(name = "adresseEnseignant", nullable = false)
	private String adresseEnseignant;
	
	@Column(name = "date_recrut", nullable = false)
	private String date_recrut;

	@OneToMany(mappedBy = "enseignant")
	private List<Matiere> matieres;
	
	@OneToMany(mappedBy = "enseignant")
	private List<ProgrammeCours> programmeCours;
	
	@OneToMany(mappedBy = "enseignant")
	private List<DispenserCours> dispensercours;



//	@OneToMany(mappedBy = "enseignant", cascade = CascadeType.ALL)
//	private List<Matiere> matieres;
//
//	@OneToMany(mappedBy = "enseignant", cascade = CascadeType.ALL)
//	private List<ProgrammeCours> programmeCours;
//
//	@OneToMany(mappedBy = "enseignant", cascade = CascadeType.ALL)
//	private List<DispenserCours> dispensercours;



	public Enseignant() {


	}

	public Enseignant(Long id, int matriculeEnseignant, String nomEnseignant, String prenomEnseignant,
			String emailEnseignant, String telephoneEnseignant, String villeEnseignant, String paysEnseignant,
			String adresseEnseignant, String date_recrut) {
		super();
		this.id = id;
		this.matriculeEnseignant = matriculeEnseignant;
		this.nomEnseignant = nomEnseignant;
		this.prenomEnseignant = prenomEnseignant;
		this.emailEnseignant = emailEnseignant;
		this.telephoneEnseignant = telephoneEnseignant;
		this.villeEnseignant = villeEnseignant;
		this.paysEnseignant = paysEnseignant;
		this.adresseEnseignant = adresseEnseignant;
		this.date_recrut = date_recrut;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMatriculeEnseignant() {
		return matriculeEnseignant;
	}

	public void setMatriculeEnseignant(int matriculeEnseignant) {
		this.matriculeEnseignant = matriculeEnseignant;
	}

	public String getNomEnseignant() {
		return nomEnseignant;
	}

	public void setNomEnseignant(String nomEnseignant) {
		this.nomEnseignant = nomEnseignant;
	}

	public String getPrenomEnseignant() {
		return prenomEnseignant;
	}

	public void setPrenomEnseignant(String prenomEnseignant) {
		this.prenomEnseignant = prenomEnseignant;
	}

	public String getEmailEnseignant() {
		return emailEnseignant;
	}

	public void setEmailEnseignant(String emailEnseignant) {
		this.emailEnseignant = emailEnseignant;
	}

	public String getAdresseEnseignant() {
		return adresseEnseignant;
	}

	public void setAdresseEnseignant(String adresseEnseignant) {
		this.adresseEnseignant = adresseEnseignant;
	}

	public String getDate_recrut() {
		return date_recrut;
	}

	public void setDate_recrut(String date_recrut) {
		this.date_recrut = date_recrut;
	}



	public String getTelephoneEnseignant() {
		return telephoneEnseignant;
	}


	public void setTelephoneEnseignant(String telephoneEnseignant) {
		this.telephoneEnseignant = telephoneEnseignant;
	}

	public String getVilleEnseignant() {
		return villeEnseignant;
	}

	public void setVilleEnseignant(String villeEnseignant) {
		this.villeEnseignant = villeEnseignant;
	}

	public String getPaysEnseignant() {
		return paysEnseignant;
	}

	public void setPaysEnseignant(String paysEnseignant) {
		this.paysEnseignant = paysEnseignant;
	}

	@Override
	public String toString() {
		return "Enseignant [id=" + id + ", MatriculeEnseignant=" + matriculeEnseignant + ", NomEnseignant="
				+ nomEnseignant + ", PrenomEnseignant=" + prenomEnseignant + ", EmailEnseignant=" + emailEnseignant
				+ ", TelephoneEnseignant=" + telephoneEnseignant + ", VilleEnseignant=" + villeEnseignant
				+ ", PaysEnseignant=" + paysEnseignant + ", AdresseEnseignant=" + adresseEnseignant + ", date_recrut="
				+ date_recrut + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adresseEnseignant, emailEnseignant, matriculeEnseignant, nomEnseignant, paysEnseignant,
				prenomEnseignant, telephoneEnseignant, villeEnseignant, date_recrut, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enseignant other = (Enseignant) obj;
		return Objects.equals(adresseEnseignant, other.adresseEnseignant)
				&& Objects.equals(emailEnseignant, other.emailEnseignant)
				&& matriculeEnseignant == other.matriculeEnseignant
				&& Objects.equals(nomEnseignant, other.nomEnseignant)
				&& Objects.equals(paysEnseignant, other.paysEnseignant)
				&& Objects.equals(prenomEnseignant, other.prenomEnseignant)
				&& Objects.equals(telephoneEnseignant, other.telephoneEnseignant)
				&& Objects.equals(villeEnseignant, other.villeEnseignant)
				&& Objects.equals(date_recrut, other.date_recrut) && Objects.equals(id, other.id);
	}


}
