package com.emedia.gestionnotesibitept.etudiant.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.emedia.gestionnotesibitept.AppartenirPromotion.entities.AppartenirPromotion;
import com.emedia.gestionnotesibitept.appreciationSanction.entities.AppreciationSanction;
import com.emedia.gestionnotesibitept.noteEvaluation.entities.NoteEvaluation;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
//@Data
//@ToString
public class Etudiant implements Serializable {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


//	@OneToMany(mappedBy = "etudiant",orphanRemoval = true, cascade = CascadeType.PERSIST)
//	private List<AppartenirPromotion> appartenirPromotion;

//	@OneToMany(mappedBy = "etudiant",orphanRemoval = true, cascade = CascadeType.PERSIST)
//	private List<NoteEvaluation> noteEvaluation;


	
	@Column(name = "matricule", nullable = false)
	private Long matricule;
	
	@Column(name = "nom_etudiant", nullable = false)
	private String nomEtudiant;
	
	@Column(name = "prenom_etudiant", nullable = false)
	private String prenomEtudiant;
	
	@Column(name = "sexe_etudiant", nullable = false)
	private String sexeEtudiant;
	
	@Column(name = "date_naissance_etudiant", nullable = false)
	private String dateNaissanceEtudiant;
	
	@Column(name = "lieu_naissance_etudiant", nullable = false)
	private String lieuNaissanceEtudiant;
	
	@Column(name = "nationalite_etudiant", nullable = false)
	private String nationaliteEtudiant;
	
	@Column(name = "adresse_etudiant", nullable = false)
	private String adresseEtudiant;
	
	@Column(name = "email_etudiant", nullable = false)
	private String emailEtudiant;
	
	@Column(name = "telephone_etudiant", nullable = false)
	private String telephoneEtudiant;
	
	@Column(name = "ville_etudiant", nullable = false)
	private String villeEtudiant;
	
	@Column(name = "pays_etudiant", nullable = false)
	private String paysEtudiant;
	
	@Column(name = "numerocni", nullable = false)
	private String numeroCNI;
	
	@Column(name = "date_inscription", nullable = false)
	private String dateInscription;

	// constructeur
	public Etudiant() {
	}

	public Etudiant(Long id, Long matricule, String nomEtudiant, String prenomEtudiant, String sexeEtudiant,
			String dateNaissanceEtudiant, String lieuNaissanceEtudiant, String nationaliteEtudiant,
			String adresseEtudiant, String emailEtudiant, String telephoneEtudiant, String villeEtudiant,
			String paysEtudiant, String numeroCNI, String dateInscription) {
		this.id = id;
		this.matricule = matricule;
		this.nomEtudiant = nomEtudiant;
		this.prenomEtudiant = prenomEtudiant;
		this.sexeEtudiant = sexeEtudiant;
		this.dateNaissanceEtudiant = dateNaissanceEtudiant;
		this.lieuNaissanceEtudiant = lieuNaissanceEtudiant;
		this.nationaliteEtudiant = nationaliteEtudiant;
		this.adresseEtudiant = adresseEtudiant;
		this.emailEtudiant = emailEtudiant;
		this.telephoneEtudiant = telephoneEtudiant;
		this.villeEtudiant = villeEtudiant;
		this.paysEtudiant = paysEtudiant;
		this.numeroCNI = numeroCNI;
		this.dateInscription = dateInscription;
	}

	// Getter

	public Long getMatricule() {
		return matricule;
	}

	public String getNomEtudiant() {
		return nomEtudiant;
	}

	public String getPrenomEtudiant() {
		return prenomEtudiant;
	}

	public String getSexeEtudiant() {
		return sexeEtudiant;
	}

	public void setMatricule(Long matricule) {
		this.matricule = matricule;
	}

	public String getDateNaissanceEtudiant() {
		return dateNaissanceEtudiant;
	}

	public String getLieuNaissanceEtudiant() {
		return lieuNaissanceEtudiant;
	}

	public String getNationaliteEtudiant() {
		return nationaliteEtudiant;
	}

	public String getAdresseEtudiant() {
		return adresseEtudiant;
	}

	public String getEmailEtudiant() {
		return emailEtudiant;
	}

	public String getTelephoneEtudiant() {
		return telephoneEtudiant;
	}

	public String getVilleEtudiant() {
		return villeEtudiant;
	}

	public String getPaysEtudiant() {
		return paysEtudiant;
	}

	public String getNumeroCNI() {
		return numeroCNI;
	}

	public String getDateInscription() {
		return dateInscription;
	}

	public Long getId() {
		return id;
	}

	// Setter

	public void setNomEtudiant(String nomEtudiant) {
		this.nomEtudiant = nomEtudiant;
	}

	public void setPrenomEtudiant(String prenomEtudiant) {
		this.prenomEtudiant = prenomEtudiant;
	}

	public void setSexeEtudiant(String sexeEtudiant) {
		this.sexeEtudiant = sexeEtudiant;
	}

	public void setDateNaissanceEtudiant(String dateNaissanceEtudiant) {
		this.dateNaissanceEtudiant = dateNaissanceEtudiant;
	}

	public void setLieuNaissanceEtudiant(String lieuNaissanceEtudiant) {
		this.lieuNaissanceEtudiant = lieuNaissanceEtudiant;
	}

	public void setNationaliteEtudiant(String nationaliteEtudiant) {
		this.nationaliteEtudiant = nationaliteEtudiant;
	}

	public void setAdresseEtudiant(String adresseEtudiant) {
		this.adresseEtudiant = adresseEtudiant;
	}

	public void setEmailEtudiant(String emailEtudiant) {
		this.emailEtudiant = emailEtudiant;
	}

	public void setTelephoneEtudiant(String telephoneEtudiant) {
		this.telephoneEtudiant = telephoneEtudiant;
	}

	public void setVilleEtudiant(String villeEtudiant) {
		this.villeEtudiant = villeEtudiant;
	}

	public void setPaysEtudiant(String paysEtudiant) {
		this.paysEtudiant = paysEtudiant;
	}

	public void setNumeroCNI(String numeroCNI) {
		this.numeroCNI = numeroCNI;
	}

	public void setDateInscription(String dateInscription) {
		this.dateInscription = dateInscription;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Etudiant [id=" + id + ", matricule=" + matricule + ", nomEtudiant=" + nomEtudiant + ", prenomEtudiant="
				+ prenomEtudiant + ", sexeEtudiant=" + sexeEtudiant + ", dateNaissanceEtudiant=" + dateNaissanceEtudiant
				+ ", lieuNaissanceEtudiant=" + lieuNaissanceEtudiant + ", nationaliteEtudiant=" + nationaliteEtudiant
				+ ", adresseEtudiant=" + adresseEtudiant + ", emailEtudiant=" + emailEtudiant + ", telephoneEtudiant="
				+ telephoneEtudiant + ", villeEtudiant=" + villeEtudiant + ", paysEtudiant=" + paysEtudiant
				+ ", numeroCNI=" + numeroCNI + ", dateInscription=" + dateInscription + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adresseEtudiant, dateInscription, dateNaissanceEtudiant, emailEtudiant, id,
				lieuNaissanceEtudiant, matricule, nationaliteEtudiant, nomEtudiant, numeroCNI, paysEtudiant,
				prenomEtudiant, sexeEtudiant, telephoneEtudiant, villeEtudiant);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etudiant other = (Etudiant) obj;
		return Objects.equals(adresseEtudiant, other.adresseEtudiant)
				&& Objects.equals(dateInscription, other.dateInscription)
				&& Objects.equals(dateNaissanceEtudiant, other.dateNaissanceEtudiant)
				&& Objects.equals(emailEtudiant, other.emailEtudiant) && Objects.equals(id, other.id)
				&& Objects.equals(lieuNaissanceEtudiant, other.lieuNaissanceEtudiant)
				&& Objects.equals(matricule, other.matricule)
				&& Objects.equals(nationaliteEtudiant, other.nationaliteEtudiant)
				&& Objects.equals(nomEtudiant, other.nomEtudiant) && Objects.equals(numeroCNI, other.numeroCNI)
				&& Objects.equals(paysEtudiant, other.paysEtudiant)
				&& Objects.equals(prenomEtudiant, other.prenomEtudiant)
				&& Objects.equals(sexeEtudiant, other.sexeEtudiant)
				&& Objects.equals(telephoneEtudiant, other.telephoneEtudiant)
				&& Objects.equals(villeEtudiant, other.villeEtudiant);
	}
	
	
}