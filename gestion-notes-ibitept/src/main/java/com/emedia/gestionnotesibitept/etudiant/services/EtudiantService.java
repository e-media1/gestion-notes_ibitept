package com.emedia.gestionnotesibitept.etudiant.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.etudiant.dao.EtudiantRepository;
import com.emedia.gestionnotesibitept.etudiant.entities.Etudiant;

@CrossOrigin("*")
@RestController
public class EtudiantService {
	
	@Autowired
	private EtudiantRepository etudiantRepository;
	
	@GetMapping(value="/listEtudiant")
	public List<Etudiant> listEtudiants() {
		return etudiantRepository.findAll();
	}
	
	@GetMapping(value="/listEtudiant/{id}")
	public Etudiant listEtudiants(@PathVariable("id") Long id){
		return etudiantRepository.findById(id).get();
	}
	
	@PutMapping(value="/updateEtudiant/{id}")
	public Etudiant update(@PathVariable("id") Long id, @RequestBody Etudiant e){
		e.setId(id); 
		return etudiantRepository.save(e);
	}
	
	@PostMapping(value="/saveEtudiant")
	public Etudiant update( @RequestBody Etudiant e){
		return etudiantRepository.save(e);
	}
	
	@DeleteMapping(value="/deleteEtudiant/{id}")
	public void delete(@PathVariable(name="id") Long id) {
		etudiantRepository.deleteById(id);
	}
	
}
