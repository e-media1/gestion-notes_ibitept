package com.emedia.gestionnotesibitept;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import com.emedia.gestionnotesibitept.enseignant.dao.EnseignantRepository;
import com.emedia.gestionnotesibitept.enseignant.entities.Enseignant;
import com.emedia.gestionnotesibitept.etudiant.dao.EtudiantRepository;
import com.emedia.gestionnotesibitept.etudiant.entities.Etudiant;
import com.emedia.gestionnotesibitept.evaluation.dao.EvaluationRepository;
import com.emedia.gestionnotesibitept.evaluation.entities.Evaluation;
import com.emedia.gestionnotesibitept.matiere.dao.MatiereRepository;
import com.emedia.gestionnotesibitept.matiere.entities.Matiere;
import com.emedia.gestionnotesibitept.noteEvaluation.dao.NoteEvaluationRepository;
import com.emedia.gestionnotesibitept.noteEvaluation.entities.NoteEvaluation;
import com.emedia.gestionnotesibitept.programmeCours.dao.ProgrammeCoursRepository;
import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;
import com.emedia.gestionnotesibitept.promotion.dao.PromotionRepository;
import com.emedia.gestionnotesibitept.promotion.entities.Promotion;
import com.emedia.gestionnotesibitept.session.dao.SessionRepository;
import com.emedia.gestionnotesibitept.session.entities.Session;
import com.emedia.gestionnotesibitept.sessionPromotion.dao.SessionPromotionRepository;
import com.emedia.gestionnotesibitept.sessionPromotion.entities.SessionPromotion;

@SpringBootApplication
public class GestionNotesIbiteptApplication implements CommandLineRunner {

	@Autowired
	private PromotionRepository promotionRepository;

	@Autowired
	private SessionPromotionRepository sessionPromotionRepository;

	@Autowired
	private EtudiantRepository etudiantRepository;

	@Autowired
	private EnseignantRepository enseignantRepository;
	
	@Autowired
	private MatiereRepository matiereRepository;
	
	@Autowired
	private ProgrammeCoursRepository programme_courRepository;
	
	@Autowired
	private EvaluationRepository evaluationRepository;
	
	@Autowired
	private SessionRepository sessionRepository;
	@Autowired
	private NoteEvaluationRepository noteEvalRepository;
	
	@Autowired
	private RepositoryRestConfiguration restConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(GestionNotesIbiteptApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		restConfiguration.exposeIdsFor(Etudiant.class);
		etudiantRepository.findAll().forEach(e -> {
			System.out.println(e.toString());
		});
		
		restConfiguration.exposeIdsFor(NoteEvaluation.class);
			noteEvalRepository.findAll().forEach(n->{
				System.out.println(n.toString());
		});
		

		// Promotion
		restConfiguration.exposeIdsFor(Promotion.class);
		promotionRepository.findAll().forEach(p -> {

			System.out.println(p.toString());
		});

		// SessionPromotion
		restConfiguration.exposeIdsFor(SessionPromotion.class);
//		sessionPromotionRepository.save(new SessionPromotion(null,1,"Premier"));
		sessionPromotionRepository.findAll().forEach(p -> {
			System.out.println(p.toString());
		});
		restConfiguration.exposeIdsFor(Enseignant.class);
		enseignantRepository.save(new Enseignant(null, 123, "toto", "ptot", "toto@gmail.com", "234567", "lome", "togo", "adidogome", "2343-34-34"));	
		enseignantRepository.findAll().forEach(e -> {
			System.out.println(e.toString());
		});

		// matiere
		restConfiguration.exposeIdsFor(Matiere.class);
		// matiereRepository.save(new Matiere(null, 123, "Bible", 4, "2012-23-12","2012-23-12",1));
		matiereRepository.findAll().forEach(e -> {
			System.out.println(e.toString());
		});

		// Programme_cours
		restConfiguration.exposeIdsFor(ProgrammeCours.class);
//		programme_courRepository.save(new ProgrammeCours(null,1,"20-06-2019","30-09-2019","Super", Matiere.class, Enseignant.class, SessionPromotion.class, null));
		programme_courRepository.findAll().forEach(e -> {
			System.out.println(e.toString());
		});

		// Evaluation
		restConfiguration.exposeIdsFor(Evaluation.class);
		// evaluationRepository.save(new Evaluation(null, "evaluation", "10-09-2021"));
		evaluationRepository.findAll().forEach(e -> {
			System.out.println(e.toString());
		});

		// Session
		restConfiguration.exposeIdsFor(Session.class);

//		sessionRepository.save(new Session(null, 2012, 34, "juin 1234", "aout 1234"));
		sessionRepository.findAll().forEach(e -> {

			System.out.println(e.toString());
		});

	}
}
