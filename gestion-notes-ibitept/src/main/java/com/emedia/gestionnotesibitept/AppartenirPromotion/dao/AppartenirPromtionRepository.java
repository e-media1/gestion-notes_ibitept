package com.emedia.gestionnotesibitept.AppartenirPromotion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.AppartenirPromotion.entities.AppartenirPromotion;
import com.emedia.gestionnotesibitept.AppartenirPromotion.entities.AppartenirPromotionKey;

@RepositoryRestResource
public interface AppartenirPromtionRepository extends JpaRepository<AppartenirPromotion, Long> {

}
