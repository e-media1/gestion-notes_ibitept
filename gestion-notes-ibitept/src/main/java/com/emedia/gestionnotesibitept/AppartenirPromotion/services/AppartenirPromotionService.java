package com.emedia.gestionnotesibitept.AppartenirPromotion.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.emedia.gestionnotesibitept.AppartenirPromotion.dao.AppartenirPromtionRepository;
import com.emedia.gestionnotesibitept.AppartenirPromotion.entities.AppartenirPromotion;




@CrossOrigin("*")
@RestController
public class AppartenirPromotionService {
	
	@Autowired
	private AppartenirPromtionRepository appartenirPromotionRepository;
	
	@GetMapping(value="/listApprPromo")
	public List<AppartenirPromotion> listApprPromo() {
		return appartenirPromotionRepository.findAll();
	}
	
	@GetMapping(value="/listApprPromo/{id}")
	public AppartenirPromotion listApprPromo(@PathVariable("id") Long id){
		return appartenirPromotionRepository.findById(id).get();
	}
	
	@PostMapping(value="/listApprPromo")
	public AppartenirPromotion save( @RequestBody AppartenirPromotion ap){
            System.out.println("appPromo ==="+ap);
		return appartenirPromotionRepository.save(ap);
	}
	
	@PutMapping(value="/listApprPromo/{id}")
	public AppartenirPromotion update(@PathVariable("id") Long id, @RequestBody AppartenirPromotion ap){
		ap.setId(id); 
		return appartenirPromotionRepository.save(ap);
	}
	
	@DeleteMapping(value="/listApprPromo/{id}")
	public void delete(@PathVariable(name="id") Long id) {
		appartenirPromotionRepository.deleteById(id);
	}
	
	
}
