package com.emedia.gestionnotesibitept.AppartenirPromotion.entities;

import java.io.Serializable;

import javax.persistence.*;


@Embeddable
public class AppartenirPromotionKey implements Serializable {
	
	@Column(name="etudiant_id")
	Long etudiantId;
	
	@Column(name="promotion_id")
	Long promotionId;
	
	public Long getEtudiantId() {
		return etudiantId;
	}

	public void setEtudiantId(Long etudiantId) {
		this.etudiantId = etudiantId;
	}
	
	public Long getPromotionId() {
		return promotionId;
	}
	
	public void setPromotion(Long promotionId) {
		this.promotionId = promotionId;
	}

	public AppartenirPromotionKey(Long etudiantId, Long promotionId) {
		super();
		this.etudiantId = etudiantId;
		this.promotionId = promotionId;
	}

	public AppartenirPromotionKey() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
