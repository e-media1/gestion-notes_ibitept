package com.emedia.gestionnotesibitept.AppartenirPromotion.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.emedia.gestionnotesibitept.etudiant.entities.Etudiant;
import com.emedia.gestionnotesibitept.promotion.entities.Promotion;



@Entity
public class AppartenirPromotion implements Serializable {
	@Id
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "dateEntree", nullable = false)
	private String dateEntree;
	
	@Column(name = "exclu", nullable = false)
	private int exclu;
	@Column(name = "motifExclusion", nullable = false)
	private String motifExclusion;
	
	@Column(name = "dateExclusion", nullable = false)
	private String dateExclusion;
	
	@Column(name = "dernierSession", nullable = false)
	private String dernierSession;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER)	
	@JoinColumn(nullable = true)
	private Etudiant etudiant;
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	private Promotion promotion;
	
	//Getters and setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDateEntree() {
		return dateEntree;
	}
	public void setDateEntree(String dateEntree) {
		this.dateEntree = dateEntree;
	}
	public int getExclu() {
		return exclu;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public Promotion getPromotion() {
		return promotion;
	}
	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}
	public void setExclu(int exclu) {
		this.exclu = exclu;
	}
	public String getMotifExclusion() {
		return motifExclusion;
	}
	public void setMotifExclusion(String motifExclusion) {
		this.motifExclusion = motifExclusion;
	}
	public String getDateExclusion() {
		return dateExclusion;
	}
	public void setDateExclusion(String dateExclusion) {
		this.dateExclusion = dateExclusion;
	}
	public String getDernierSession() {
		return dernierSession;
	}
	public void setDernierSession(String dernierSession) {
		this.dernierSession = dernierSession;
	}
	//Constructors
	public AppartenirPromotion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AppartenirPromotion(Long id, String dateEntree, int exclu, String motifExclusion, String dateExclusion,
			String dernierSession, Etudiant etudiant, Promotion promotion) {
		super();
		this.id = id;
		this.dateEntree = dateEntree;
		this.exclu = exclu;
		this.motifExclusion = motifExclusion;
		this.dateExclusion = dateExclusion;
		this.dernierSession = dernierSession;
		this.etudiant = etudiant;
		this.promotion = promotion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateEntree == null) ? 0 : dateEntree.hashCode());
		result = prime * result + ((dateExclusion == null) ? 0 : dateExclusion.hashCode());
		result = prime * result + ((dernierSession == null) ? 0 : dernierSession.hashCode());
		result = prime * result + ((etudiant == null) ? 0 : etudiant.hashCode());
		result = prime * result + exclu;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((motifExclusion == null) ? 0 : motifExclusion.hashCode());
		result = prime * result + ((promotion == null) ? 0 : promotion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppartenirPromotion other = (AppartenirPromotion) obj;
		if (dateEntree == null) {
			if (other.dateEntree != null)
				return false;
		} else if (!dateEntree.equals(other.dateEntree))
			return false;
		if (dateExclusion == null) {
			if (other.dateExclusion != null)
				return false;
		} else if (!dateExclusion.equals(other.dateExclusion))
			return false;
		if (dernierSession == null) {
			if (other.dernierSession != null)
				return false;
		} else if (!dernierSession.equals(other.dernierSession))
			return false;
		if (etudiant == null) {
			if (other.etudiant != null)
				return false;
		} else if (!etudiant.equals(other.etudiant))
			return false;
		if (exclu != other.exclu)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (motifExclusion == null) {
			if (other.motifExclusion != null)
				return false;
		} else if (!motifExclusion.equals(other.motifExclusion))
			return false;
		if (promotion == null) {
			if (other.promotion != null)
				return false;
		} else if (!promotion.equals(other.promotion))
			return false;
		return true;
	}
	//toString
	@Override
	public String toString() {
		return "AppartenirPromotion [id=" + id + ", dateEntree=" + dateEntree + ", exclu=" + exclu + ", motifExclusion="
				+ motifExclusion + ", dateExclusion=" + dateExclusion + ", dernierSession=" + dernierSession
				+ ", etudiant=" + etudiant + ", promotion=" + promotion + "]";
	}
	
	
	
		
}