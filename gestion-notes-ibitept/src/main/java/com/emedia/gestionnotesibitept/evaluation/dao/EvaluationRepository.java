package com.emedia.gestionnotesibitept.evaluation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emedia.gestionnotesibitept.evaluation.entities.Evaluation;


@RepositoryRestResource
public interface EvaluationRepository extends JpaRepository<Evaluation, Long> {

}
