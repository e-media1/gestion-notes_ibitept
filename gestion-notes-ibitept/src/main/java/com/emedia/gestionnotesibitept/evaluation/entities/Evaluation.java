package com.emedia.gestionnotesibitept.evaluation.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;

import com.emedia.gestionnotesibitept.programmeCours.entities.ProgrammeCours;

@Entity
public class Evaluation implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "titreEvaluation", nullable = false, unique = true)
	private String titreEvaluation;
	
	@Column(name = "dateEvaluation", nullable = false)
	private String dateEvaluation;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = true)
	public ProgrammeCours programmeCours;
//
//	@OneToMany(mappedBy = "evaluation")
//	public List<NoteEvaluation> noteEvaluation;


	public Evaluation() {
		super();
	}

	public Evaluation(Long id, String titreEvaluation, String dateEvaluation, ProgrammeCours programmeCours) {
		super();
		this.id = id;
		this.titreEvaluation = titreEvaluation;
		this.dateEvaluation = dateEvaluation;
		this.programmeCours = programmeCours;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitreEvaluation() {
		return titreEvaluation;
	}
	
	public void setTitreEvaluation(String titreEvaluation) {
		this.titreEvaluation = titreEvaluation;
	}

	public void setProgrammeCours(ProgrammeCours programmeCours) {
		this.programmeCours = programmeCours;
	}

	public ProgrammeCours getProgrammeCours() {
		return programmeCours;
	}

	public String getDateEvaluation() {
		return this.dateEvaluation;
	}

	public void setDateEvaluation(String dateEvaluation) {
		this.dateEvaluation = dateEvaluation;
	}

	@Override
	public String toString() {
		return "Evaluation [id=" + id + ", titreEvaluation=" + titreEvaluation + ", dateEvaluation=" + dateEvaluation
				+ ", programmeCours=" + programmeCours + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(dateEvaluation, id, programmeCours, titreEvaluation);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evaluation other = (Evaluation) obj;
		return Objects.equals(dateEvaluation, other.dateEvaluation) && Objects.equals(id, other.id)
				&& Objects.equals(programmeCours, other.programmeCours)
				&& Objects.equals(titreEvaluation, other.titreEvaluation);
	}
	
	

}
