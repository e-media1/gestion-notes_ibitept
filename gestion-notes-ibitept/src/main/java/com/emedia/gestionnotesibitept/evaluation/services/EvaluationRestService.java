package com.emedia.gestionnotesibitept.evaluation.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.emedia.gestionnotesibitept.evaluation.dao.EvaluationRepository;
import com.emedia.gestionnotesibitept.evaluation.entities.Evaluation;
import com.emedia.gestionnotesibitept.programmeCours.dao.ProgrammeCoursRepository;


@CrossOrigin("*")
@RestController
public class EvaluationRestService {

	@Autowired
	EvaluationRepository evaluationRepository;

	@Autowired
	ProgrammeCoursRepository programmeRepository;

	@GetMapping(value = "/listEvaluation")
	public List<Evaluation> listEvaluation() {
		return evaluationRepository.findAll();
	}

	@GetMapping(value = "/listEvaluation/{id}")
	public Evaluation listEvaluation(@PathVariable(name = "id") Long id) {
		return evaluationRepository.findById(id).get();
	}

	@PutMapping(value = "/updateEvaluation/{id}")
	public Evaluation updateEvaluation(@PathVariable(name = "id") Long id, @RequestBody Evaluation ev) {
		ev.setId(id);
		return evaluationRepository.save(ev);
	}

	@PostMapping(value = "/saveEvaluation")
	public Evaluation saveEvaluation(@RequestBody Evaluation ev) {
//		ev.setProgrammeCours(this.programmeRepository.getById(ev.programmeCours.getId()));
//		System.out.println("ev.id ==="+ev.getProgrammeCours().getId());
//		ev.setProgrammeCours(null);
//		System.out.println("ev ==="+ev.toString());
		return evaluationRepository.save(ev);

	}


	@DeleteMapping(value = "/deleteEvaluation/{id}")
	public void deleteEvaluation(@PathVariable(name = "id") Long id) {
		evaluationRepository.deleteById(id);
	}
}
